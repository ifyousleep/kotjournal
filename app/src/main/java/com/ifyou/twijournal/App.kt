package com.ifyou.twijournal

import android.app.Application
import android.content.Context
import com.ifyou.twijournal.net.API
import com.ifyou.twijournal.utils.TypefaceUtil
import com.ifyou.twijournal.utils.setNightMode

import java.io.File
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

var appContext: Context? = null

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        TypefaceUtil.overrideFont(applicationContext, "SERIF", "fonts/Roboto.ttf")

        var client = OkHttpClient()
        val cacheSize = 10 * 1024 * 2048 // 20 MiB
        val cacheDirectory = File(cacheDir.absolutePath, "TwiJournalAppHttpCache")
        val cache = Cache(cacheDirectory, cacheSize.toLong())
        client = client
                .newBuilder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .cache(cache)
                .build()

        val retrofit = Retrofit.Builder()
                .client(client)
                .baseUrl("https://api.tjournal.ru/2.3/")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        service = retrofit.create(API::class.java)
        appContext = applicationContext
        setNightMode()
    }

    companion object {
        private var service: API by Delegates.notNull()

        fun API(): API {
            return service
        }
    }
}
