package com.ifyou.twijournal.activities

import android.annotation.TargetApi
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.ifyou.twijournal.R
import com.ifyou.twijournal.utils.fadeIn
import com.ifyou.twijournal.utils.fadeOut
import kotlinx.android.synthetic.main.activity_club_item.*

class ClubItemActivity : AppCompatActivity() {

    private var URL = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_club_item)

        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorWhite))
        setSupportActionBar(toolbar)

        supportActionBar!!.title = getString(R.string.post)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        val extras = intent.extras
        if (extras != null) {
            URL = extras.getString("URL")
        }
        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        webView.fadeOut(false)
        webView.loadUrl(URL + "?mobile=android&v=5")
        /*webView.setWebChromeClient(object : WebChromeClient() {
            override fun onReceivedTitle(view: WebView?, title: String?) {
                super.onReceivedTitle(view, title)
                supportActionBar?.title = title
            }
        })*/
        webView.setWebViewClient(object : WebViewClient() {
            @SuppressWarnings("deprecation")
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                val uri: Uri = Uri.parse(url)
                return handleUri(uri)
            }

            @TargetApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(view: WebView, url: WebResourceRequest): Boolean {
                val uri: Uri = url.url
                return handleUri(uri)
            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                progressBar.fadeOut(true)
                webView.fadeIn(true)
            }

            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        webView.destroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.share -> {
                val s = Intent(android.content.Intent.ACTION_SEND)
                s.type = "text/plain"
                s.putExtra(Intent.EXTRA_SUBJECT, "Via TwiJournal")
                s.putExtra(Intent.EXTRA_TEXT, URL)
                startActivity(Intent.createChooser(s, "Choose an app"))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun handleUri(uri: Uri): Boolean {
        if (uri.toString().startsWith("http:") || uri.toString().startsWith("https:")) {
            if (uri.toString().endsWith(".jpg", true) || uri.toString().endsWith(".png", true)) {
                val urlStringArray = arrayListOf<String>()
                urlStringArray.add(uri.toString())
                val intent = Intent(this, ViewActivity::class.java)
                intent.putExtra("KEY", urlStringArray)
                intent.putExtra("POS", 0)
                startActivity(intent)
            } else {
                val intent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(intent)
            }
            return true
        } else
            return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.gif_menu, menu)
        return true
    }

/*    private fun onGetObservable(): Observable<ClubItemModel> {
        return App.API().getClubItem(entryId = pos)
    }

    private fun fetchData() {
        @Suppress("UNCHECKED_CAST")
        (onGetObservable() as Observable<Any>)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(object : Subscriber<Any>() {
                    override fun onCompleted() {
                    }

                    override fun onNext(responseData: Any) {
                        progressBar.fadeOut(true)
                        data = responseData
                        setData((data as ClubItemModel).entryJSON)
                    }

                    override fun onError(e: Throwable) {
                        progressBar.fadeOut(true)
                        e.printStackTrace()
                    }
                })
    }

    private fun setData(text: String) {
        textView.text = text
    }*/
}
