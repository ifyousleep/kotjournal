package com.ifyou.twijournal.activities

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import android.widget.MediaController
import com.ifyou.twijournal.R
import com.ifyou.twijournal.utils.AudioServiceActivityLeak
import com.ifyou.twijournal.utils.fadeOut
import kotlinx.android.synthetic.main.activity_gif.*

class GifActivity : AppCompatActivity() {

    private var URL = ""
    private var position = 0
    private var mediaControls: MediaController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppThemeBlack)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gif)

        val extras = intent.extras
        if (extras != null) {
            URL = extras.getString("URL")
        }

        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorWhite))
        setSupportActionBar(toolbar)

        supportActionBar!!.title = getString(R.string.gif)

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        if (mediaControls == null) {
            mediaControls = MediaController(GifActivity@this)
        }
        videoView.setMediaController(mediaControls)
        videoView.setVideoURI(Uri.parse(URL))
        videoView.setOnPreparedListener {
            mp ->
            mp.isLooping = true
            gif_progress_bar.fadeOut(true)
        }
        if (position == 0)
            videoView.start()
        else
            videoView.pause()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.share -> {
                val s = Intent(android.content.Intent.ACTION_SEND)
                s.type = "text/plain"
                s.putExtra(Intent.EXTRA_SUBJECT, "Via TwiJournal")
                s.putExtra(Intent.EXTRA_TEXT, URL)
                startActivity(Intent.createChooser(s, "Choose an app"))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        videoView.pause() // Stop GIF
        videoView.stopPlayback() // Release media player
        super.onDestroy()
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState.putInt("Position", videoView.currentPosition)
        videoView.pause()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        position = savedInstanceState.getInt("Position")
        videoView.seekTo(position)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.gif_menu, menu)
        return true
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(AudioServiceActivityLeak.preventLeakOf(base))
    }
}
