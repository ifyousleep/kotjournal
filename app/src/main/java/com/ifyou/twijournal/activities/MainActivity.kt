package com.ifyou.twijournal.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.preference.PreferenceManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.*
import com.ifyou.twijournal.R
import com.ifyou.twijournal.adapters.NavAdapter
import com.ifyou.twijournal.fragments.PagerFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var isBlackTheme = "0"
    private var isFont = "14"
    private var sharedPreferences: SharedPreferences? = null
    private var handler: Handler = Handler() {
        handleMessage(it)
    }
    lateinit private var drawerToggle: ActionBarDrawerToggle
    private var currentType = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        if (savedInstanceState == null) {
            setFragment(0)
        } else {
            toolbar_title.text = savedInstanceState.getString("title")
            currentType = savedInstanceState.getInt("current", 0)
        }
        val TITLES = arrayOf(getString(R.string.tweets), getString(R.string.popular), getString(R.string.settings))
        val ICONS = intArrayOf(R.drawable.ic_view_list_black_18dp, R.drawable.ic_whatshot_black_18dp, R.drawable.ic_settings_black_18dp)
        val NAME = "koTJournal"
        val EMAIL = "1.3"
        val PROFILE = R.drawable.hi_res
        drawerToggle = ActionBarDrawerToggle(this, drawer_layout, R.string.drawer_open, R.string.drawer_close)
        drawer_layout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()
        drawerRecyclerView.setHasFixedSize(true)
        val adapter = NavAdapter(TITLES, ICONS, NAME, EMAIL, PROFILE)
        drawerRecyclerView.adapter = adapter
        val mLayoutManager = LinearLayoutManager(this)
        drawerRecyclerView.layoutManager = mLayoutManager
        adapter.selected_item = currentType + 1
        drawerRecyclerView.addOnItemTouchListener(RecyclerTouchListener(this, object : ClickListener {
            override fun onClick(view: View, position: Int) {
                drawerRecyclerView.adapter.notifyDataSetChanged()
                drawer_layout.closeDrawer(GravityCompat.START)
                when (position) {
                    1 -> {
                        showAppBar()
                        setFragment(0)
                        adapter.selected_item = position
                        currentType = 0
                    }
                    2 -> {
                        setClubFragment(0)
                        adapter.selected_item = position
                        currentType = 1
                    }
                    3 -> {
                        adapter.selected_item = currentType + 1
                        startActivity(Intent(this@MainActivity, SettingsActivity ::class.java))
                    }
                }
            }
        }
        ))

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        isBlackTheme = sharedPreferences!!.getString("pref_theme", "0")
        isFont = sharedPreferences!!.getString("pref_font", "14")

        toolbar_title.setOnClickListener(
                { view ->
                    if (currentType == 0) {
                        val menuItemView = topView
                        val popupMenu = PopupMenu(this, menuItemView)
                        popupMenu.inflate(R.menu.main_menu)
                        popupMenu.setOnMenuItemClickListener {
                            menu ->
                            when (menu.itemId) {
                                R.id.all_t -> setFragment(0)
                                R.id.sport -> setFragment(1)
                                R.id.press -> setFragment(2)
                            }
                            true
                        }
                        popupMenu.show()
                    }
                }
        )
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("title", toolbar_title.text.toString())
        outState.putInt("current", currentType)
        super.onSaveInstanceState(outState)
    }

    fun handleMessage(msg: Message): Boolean {
        if (msg.what === 5)
            recreate()
        return true
    }

    private fun setClubFragment(set: Int = 0) {
        showAppBar()
        toolbar_title.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        toolbar_title.text = getString(R.string.popular)
        val existing = supportFragmentManager.findFragmentById(R.id.container)
        if (existing != null) {
            supportFragmentManager.beginTransaction().remove(existing).commit()
        }
        supportFragmentManager.beginTransaction()
                .add(R.id.container, PagerFragment.newInstance(set, 1))
                .commit()
    }

    private fun setFragment(set: Int = 0) {
        toolbar_title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down_white_24dp, 0)
        when (set) {
            1 -> {
                toolbar_title.text = getString(R.string.sport)
            }
            2 -> {
                toolbar_title.text = getString(R.string.press)
            }
            else -> {
                toolbar_title.text = getString(R.string.all)
            }
        }

        val existing = supportFragmentManager.findFragmentById(R.id.container)
        if (existing != null) {
            supportFragmentManager.beginTransaction().remove(existing).commit()
        }
        supportFragmentManager.beginTransaction()
                .add(R.id.container, PagerFragment.newInstance(set, 0))
                .commit()
    }

    override fun onResume() {
        super.onResume()
        val onIsBlack = sharedPreferences!!.getString("pref_theme", "0")
        val onIsFont = sharedPreferences!!.getString("pref_font", "14")
        if (onIsBlack != isBlackTheme || onIsFont != isFont) {
            isBlackTheme = onIsBlack
            isFont = onIsFont
            val msg = handler.obtainMessage()
            msg.what = 5
            handler.sendMessage(msg)
            recreate()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.general_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.about -> {
                AlertDialog.Builder(this)
                        .setTitle(getString(R.string.title))
                        .setMessage(getString(R.string.ifyou))
                        .show()
                return true
            }
        }
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun showAppBar() {
        appBarLayout.setExpanded(true, true)
    }

    interface ClickListener {
        fun onClick(view: View, position: Int)
    }

    class RecyclerTouchListener(context: Context, private val clickListener: ClickListener?) : RecyclerView.OnItemTouchListener {

        private val gestureDetector: GestureDetector

        init {
            gestureDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapUp(e: MotionEvent): Boolean {
                    return true
                }
            })
        }

        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
            val child = rv.findChildViewUnder(e.x, e.y)
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildAdapterPosition(child))
            }
            return false
        }

        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
        }

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        }
    }
}
