package com.ifyou.twijournal.activities

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.*
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.ifyou.twijournal.R
import com.ifyou.twijournal.ui.SwipeDismissTouchListener
import com.ifyou.twijournal.utils.fadeOut
import kotlinx.android.synthetic.main.activity_view.*
import org.jetbrains.anko.act
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class ViewActivity : AppCompatActivity() {

    private var pos: Int = 0
    private var mResources = arrayListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppThemeBlack)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view)

        val extras = intent.extras

        if (extras != null) {
            mResources = extras.getStringArrayList("KEY")
            pos = extras.getInt("POS")
        }

        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorWhite))
        setSupportActionBar(toolbar)

        supportActionBar!!.title = (pos + 1).toString() + " / " + mResources.count().toString()
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        val mCustomPagerAdapter = CustomPagerAdapter(this)
        pager.adapter = mCustomPagerAdapter
        pager.currentItem = pos
        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                pos = position
                (act as AppCompatActivity)
                        .supportActionBar!!.title = (position + 1).toString() + " / " + mResources.count().toString()
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.image_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.copyUrl -> {
                val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clip = ClipData.newPlainText("", mResources[pos])
                clipboard.primaryClip = clip
                toast(getString(R.string.succes))
                return true
            }
            R.id.share -> {
                sharePic()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun sharePic() {
        val c = Calendar.getInstance()
        val offset = c.get(Calendar.ZONE_OFFSET) + c.get(Calendar.DST_OFFSET)
        val tmpStr10 = java.lang.Long.toString((c.timeInMillis + offset) % (24 * 60 * 60 * 1000))
        doAsync {
            val theBitmap = Glide
                    .with(applicationContext)
                    .load(mResources[pos])
                    .asBitmap()
                    .into(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .get()
            uiThread {
                try {
                    val cachePath = File(cacheDir, "images")
                    cachePath.mkdirs() // don't forget to make the directory
                    val stream = FileOutputStream(cachePath.toString() + "/" + tmpStr10 + ".png") // overwrites this image every time
                    theBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                    stream.close()
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                val imagePath = File(cacheDir, "images")
                val newFile = File(imagePath, tmpStr10 + ".png")
                val contentUri = FileProvider.getUriForFile(applicationContext, "com.ifyou.twijournal.fileprovider", newFile)
                if (contentUri != null) {
                    val shareIntent = Intent()
                    shareIntent.action = Intent.ACTION_SEND
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION) // temp permission for receiving app to read this file
                    shareIntent.setDataAndType(contentUri, contentResolver.getType(contentUri))
                    shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri)
                    shareIntent.putExtra(Intent.EXTRA_TEXT, "Shared via TwiJournal")
                    startActivity(Intent.createChooser(shareIntent, "Choose an app"))

                }
            }
        }
    }

    var dismissCallbacks: SwipeDismissTouchListener.DismissCallbacks = object : SwipeDismissTouchListener.DismissCallbacks {
        override fun canDismiss(token: Any): Boolean {
            return true
        }

        override fun onDismiss(view: View, token: Any) {
            remove()
        }
    }

    fun remove() {
        viewerLayout.removeAllViews()
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition()
        } else
            finish()
    }

    internal inner class CustomPagerAdapter(mContext: Context) : PagerAdapter() {

        var mLayoutInflater: LayoutInflater

        init {
            mLayoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        override fun getCount(): Int {
            return mResources.count()
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object` as FrameLayout
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false)
            val imageView = itemView.findViewById(R.id.imageView) as ImageView
            val progressBar = itemView.findViewById(R.id.pb) as ProgressBar

            //val frame = itemView.findViewById(R.id.frameLayout)
            val swipeDismissTouchListener = SwipeDismissTouchListener(imageView, "", dismissCallbacks)
            swipeDismissTouchListener.setOrientation(SwipeDismissTouchListener.SWIPE_VERTICAL)
            imageView.setOnTouchListener(swipeDismissTouchListener)

            /*val frame = itemView.findViewById(R.id.frameLayout)

            val swipe = SwipeDismissBehavior<View>()
            swipe.setSwipeDirection(SwipeDismissBehavior.STATE_DRAGGING)
            swipe.setListener(object : SwipeDismissBehavior.OnDismissListener {
                override fun onDragStateChanged(state: Int) {

                }

                override fun onDismiss(view: View?) {
                    Toast.makeText(applicationContext, "Card swiped !!", Toast.LENGTH_SHORT).show()
                }
            })
            val coordinatorParams:CoordinatorLayout.LayoutParams = frame.layoutParams as CoordinatorLayout.LayoutParams
            coordinatorParams.behavior = swipe*/

            val d: Drawable
            d = ColorDrawable(ContextCompat.getColor(container.context, android.R.color.transparent))

            Glide.with(this@ViewActivity)
                    .load(mResources[position])
                    .asBitmap()
                    .placeholder(d)
                    .fitCenter()
                    .into(object : BitmapImageViewTarget(imageView) {
                        override fun setResource(resource: Bitmap) {
                            imageView.setImageBitmap(resource)
                            progressBar.fadeOut(true)
                        }
                    })
            container.addView(itemView)

            imageView.setOnClickListener {
                val toolbar = (container.context as AppCompatActivity).supportActionBar
                if (toolbar!!.isShowing)
                    toolbar.hide()
                else
                    toolbar.show()
            }

            return itemView
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as FrameLayout)
        }
    }
}
