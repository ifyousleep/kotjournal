package com.ifyou.twijournal.adapters

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v7.preference.PreferenceManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ifyou.twijournal.R
import com.ifyou.twijournal.activities.ClubItemActivity
import com.ifyou.twijournal.activities.ViewActivity
import com.ifyou.twijournal.appContext
import com.ifyou.twijournal.net.Club
import com.ifyou.twijournal.utils.ClickableMovementMethod
import com.ifyou.twijournal.utils.loadImage
import com.ifyou.twijournal.utils.loadImageTweet
import kotlinx.android.synthetic.main.club_footer.view.*
import kotlinx.android.synthetic.main.item_club.view.*
import kotlinx.android.synthetic.main.item_one_image.view.*

class ClubAdapter(var context: Context, var data: List<Any>) : RecyclerView.Adapter<ClubAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClubAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_club, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ClubAdapter.ViewHolder, position: Int) {
        holder.bindData(data[position] as Club, context)
    }

    fun updateTweetsList(tweets: List<Any>) {
        data += tweets
        this.notifyDataSetChanged()
    }

    override fun getItemCount() = data.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindData(item: Club, context: Context) {
            val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext)
            val textSize = sharedPreferences.getString("pref_font", "14")
            val textColor = ContextCompat.getColor(context, R.color.colorPrimary)

            val urlStringArray = arrayListOf<String>()

            val result: Spanned
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                result = Html.fromHtml(item.intro.replace("<p>", "").replace("</p>", "\n").replace("\n", "<br />"), Html.FROM_HTML_MODE_LEGACY)
            } else {
                @Suppress("DEPRECATION")
                result = Html.fromHtml(item.intro.replace("<p>", "").replace("</p>", "\n").replace("\n", "<br />"))
            }
            itemView.itemText.textSize = textSize.toFloat()
            itemView.itemText.text = result
            itemView.itemText.movementMethod = ClickableMovementMethod.instance
            itemView.itemText.isFocusable = false
            itemView.itemText.isClickable = false
            itemView.itemText.isLongClickable = false
            itemView.itemTitle.textSize = textSize.toFloat() + 2
            itemView.itemTitle.text = item.title

            itemView.favorite.setTextColor(textColor)
            itemView.favorite.text = item.likes.summ.toString()

            if (item.publicAuthor != null)
                itemView.share.loadImage(context, item.publicAuthor.profile_image_url)
            else
                itemView.share.visibility = View.GONE

            if (item.cover != null) {
                urlStringArray.add(item.cover.url)
                if (item.cover.type == 2) {
                    itemView.video_icon.visibility = View.VISIBLE
                    itemView.one_image.visibility = View.VISIBLE
                    itemView.one_image.loadImageTweet(context, item.cover.thumbnailUrl, R.drawable.place)
                    itemView.one_image.setOnClickListener(
                            { view ->
                                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(item.cover.url))
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                context.startActivity(intent)
                            }
                    )
                } else if (item.cover.type == 1) {
                    itemView.one_image.visibility = View.VISIBLE
                    itemView.video_icon.visibility = View.GONE
                    itemView.one_image.loadImageTweet(context, item.cover.thumbnailUrl, R.drawable.place)
                    itemView.one_image.setOnClickListener(
                            { view ->
                                val intent = Intent(context, ViewActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                intent.putExtra("KEY", urlStringArray)
                                intent.putExtra("POS", 0)
                                context.startActivity(intent)
                            }
                    )
                } else {
                    itemView.one_image.visibility = View.GONE
                    itemView.video_icon.visibility = View.GONE
                }

            } else {
                itemView.one_image.visibility = View.GONE
                itemView.video_icon.visibility = View.GONE
            }

            itemView.card.setOnClickListener(
                    { view ->
                        val intent = Intent(context, ClubItemActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        intent.putExtra("URL", item.url)
                        context.startActivity(intent)
                    }
            )
        }
    }
}
