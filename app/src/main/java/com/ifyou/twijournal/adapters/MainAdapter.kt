package com.ifyou.twijournal.adapters

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v7.preference.PreferenceManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ifyou.twijournal.R
import com.ifyou.twijournal.activities.GifActivity
import com.ifyou.twijournal.activities.ViewActivity
import com.ifyou.twijournal.appContext
import com.ifyou.twijournal.net.Post
import com.ifyou.twijournal.utils.*
import kotlinx.android.synthetic.main.item_footer.view.*
import kotlinx.android.synthetic.main.item_four_image.view.*
import kotlinx.android.synthetic.main.item_one_image.view.*
import kotlinx.android.synthetic.main.item_three_image.view.*
import kotlinx.android.synthetic.main.item_tweet.view.*
import kotlinx.android.synthetic.main.item_two_image.view.*
import java.util.*

class MainAdapter(var context: Context, var data: List<Any>) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_tweet, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: MainAdapter.ViewHolder, position: Int) {
        holder.bindData(data[position] as Post, context)
    }

    fun updateTweetsList(tweets: List<Any>) {
        data += tweets
        this.notifyDataSetChanged()
    }

    override fun getItemCount() = data.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        @SuppressWarnings("deprecation")
        fun bindData(item: Post, context: Context) {
            var countMedia = 0
            if (item.has_media) {
                countMedia = item.media.count()
            }
            val textColor = ContextCompat.getColor(context, R.color.colorPrimary)
            val result: Spanned
            val text = replaceWebLink(item.text)
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                result = Html.fromHtml(text.replace("\n", "<br />"), Html.FROM_HTML_MODE_LEGACY)
            } else {
                @Suppress("DEPRECATION")
                result = Html.fromHtml(text.replace("\n", "<br />"))
            }
            val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext)
            val textSize = sharedPreferences.getString("pref_font", "14")
            itemView.card.fadeIn(true)
            itemView.itemName.textSize = textSize.toFloat()
            itemView.itemName.text = item.user.name + " (@" + item.user.screen_name + ")"
            itemView.itemText.textSize = textSize.toFloat()
            itemView.itemText.text = result
            itemView.itemText.movementMethod = ClickableMovementMethod.instance
            itemView.itemText.isFocusable = false
            itemView.itemText.isClickable = false
            itemView.itemText.isLongClickable = false
            itemView.itemDate.textSize = textSize.toFloat() - 4
            itemView.itemDate.text = getDate(item.created_at)
            itemView.favorite.setTextColor(textColor)
            itemView.favorite.text = item.favorite_count.toString()
            itemView.comments.setTextColor(textColor)
            itemView.comments.text = item.retweet_count.toString()
            itemView.avatar.loadImage(context, item.user.profile_image_url)

            itemView.frame_one_image.visibility = if (countMedia == 1) View.VISIBLE else View.GONE
            itemView.frame_two_image.visibility = if (countMedia == 2) View.VISIBLE else View.GONE
            itemView.frame_three_image.visibility = if (countMedia == 3) View.VISIBLE else View.GONE
            itemView.frame_four_image.visibility = if (countMedia == 4) View.VISIBLE else View.GONE

            val urlStringArray = arrayListOf<String>()
            if (item.has_media) {
                for (i in 0..item.media.size - 1) {
                    if (!item.media[i].media_url.endsWith(".mp4")
                            && item.media[i].type != 2)
                        urlStringArray.add(item.media[i].media_url)
                }
            }

            when (countMedia) {
                1 -> {
                    itemView.one_image.loadImageTweet(context, item.media[0].thumbnail_url, R.drawable.place)
                    if (item.media[0].type == 2) {
                        itemView.video_icon.visibility = View.VISIBLE
                        itemView.one_image.setOnClickListener(
                                { view ->
                                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(item.media[0].media_url))
                                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                    context.startActivity(intent)
                                }
                        )
                    } else if (item.media[0].media_url.endsWith(".mp4")) {
                        itemView.video_icon.visibility = View.VISIBLE
                        itemView.one_image.setOnClickListener(
                                { view ->
                                    val intent = Intent(context, GifActivity::class.java)
                                    intent.putExtra("URL", item.media[0].media_url)
                                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                    context.startActivity(intent)
                                }
                        )
                    } else {
                        itemView.video_icon.visibility = View.GONE
                        itemView.one_image.setOnClickListener(
                                { view ->
                                    val intent = Intent(context, ViewActivity::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                    intent.putExtra("KEY", urlStringArray)
                                    intent.putExtra("POS", 0)
                                    context.startActivity(intent)
                                }
                        )
                    }
                }
                2 -> {
                    itemView.one_image_two.loadImageTweet(context, item.media[0].thumbnail_url, R.drawable.place)
                    itemView.two_image_two.loadImageTweet(context, item.media[1].thumbnail_url, R.drawable.place)
                }
                3 -> {
                    itemView.one_image_two_three.loadImageTweet(context, item.media[0].thumbnail_url, R.drawable.place3)
                    itemView.two_image_two_three.loadImageTweet(context, item.media[1].thumbnail_url, R.drawable.place3)
                    itemView.three_image_three.loadImageTweet(context, item.media[2].thumbnail_url, R.drawable.place3)
                }
                4 -> {
                    itemView.one_image_two_three_four.loadImageTweet(context, item.media[0].thumbnail_url, R.drawable.place3)
                    itemView.two_image_two_three_four.loadImageTweet(context, item.media[1].thumbnail_url, R.drawable.place3)
                    itemView.three_image_two_four.loadImageTweet(context, item.media[2].thumbnail_url, R.drawable.place3)
                    itemView.four_image_two_four.loadImageTweet(context, item.media[3].thumbnail_url, R.drawable.place3)
                }
            }

            itemView.card.setOnClickListener(
                    { view ->

                    }
            )

            itemView.share.setOnClickListener(
                    { view ->
                        val s = Intent(android.content.Intent.ACTION_SEND)
                        s.type = "text/plain"
                        s.putExtra(Intent.EXTRA_TEXT, result.toString() + "\n" + "https://twitter.com/"
                                + item.user.screen_name + "/status/" + item.id)
                        s.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        context.startActivity(Intent.createChooser(s, "Choose an app"))
                    }
            )

            itemView.favorite.setOnClickListener(
                    { view ->
                        val path = "https://twitter.com/intent/favorite?tweet_id=" + item.id + "&related=twijournal"
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(path))
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        context.startActivity(intent)
                    }
            )

            itemView.comments.setOnClickListener(
                    { view ->
                        val path = "https://twitter.com/intent/retweet?tweet_id=" + item.id + "&related=twijournal"
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(path))
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        context.startActivity(intent)
                    }
            )

            itemView.avatar.setOnClickListener(
                    { view ->
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + item.user.screen_name))
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        context.startActivity(intent)
                    }
            )

            itemView.one_image_two.setOnClickListener(
                    { view -> startViewer(urlStringArray, 0, context) }
            )
            itemView.two_image_two.setOnClickListener(
                    { view -> startViewer(urlStringArray, 1, context) }
            )

            itemView.one_image_two_three.setOnClickListener(
                    { view -> startViewer(urlStringArray, 0, context) }
            )
            itemView.two_image_two_three.setOnClickListener(
                    { view -> startViewer(urlStringArray, 1, context) }
            )
            itemView.three_image_three.setOnClickListener(
                    { view -> startViewer(urlStringArray, 2, context) }
            )

            itemView.one_image_two_three_four.setOnClickListener(
                    { view -> startViewer(urlStringArray, 0, context) }
            )
            itemView.two_image_two_three_four.setOnClickListener(
                    { view -> startViewer(urlStringArray, 1, context) }
            )
            itemView.three_image_two_four.setOnClickListener(
                    { view -> startViewer(urlStringArray, 2, context) }
            )
            itemView.four_image_two_four.setOnClickListener(
                    { view -> startViewer(urlStringArray, 3, context) }
            )
        }

        private fun startViewer(urlArray: ArrayList<String>, pos: Int, context: Context) {
            val intent = Intent(context, ViewActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("KEY", urlArray)
            intent.putExtra("POS", pos)
            context.startActivity(intent)
        }
    }
}