package com.ifyou.twijournal.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.ifyou.twijournal.R

class NavAdapter(private val mNavTitles: Array<String>,
                 private val mIcons: IntArray,
                 private val name: String,
                 private val email: String,
                 private val profile: Int) : RecyclerView.Adapter<NavAdapter.ViewHolder>() {

    var selected_item = 1

    class ViewHolder(itemView: View, ViewType: Int) : RecyclerView.ViewHolder(itemView){
        internal var Holderid: Int = 0
        internal var textView: TextView? = null
        internal var imageView: ImageView? = null
        internal var profile: ImageView? = null
        internal var Name: TextView? = null
        internal var email: TextView? = null
        internal var nav_holder: LinearLayout? = null

        init {
            itemView.isClickable = true
            if (ViewType == TYPE_ITEM) {
                textView = itemView.findViewById(R.id.rowText) as TextView
                imageView = itemView.findViewById(R.id.rowIcon) as ImageView
                nav_holder = itemView.findViewById(R.id.nav_holder) as LinearLayout
                Holderid = 1
            } else {
                Name = itemView.findViewById(R.id.name) as TextView
                email = itemView.findViewById(R.id.email) as TextView
                profile = itemView.findViewById(R.id.circleView) as ImageView
                Holderid = 0
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NavAdapter.ViewHolder? {
        if (viewType == TYPE_ITEM) {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.nav_item, parent, false)
            val vhItem = ViewHolder(v, viewType)
            return vhItem

        } else if (viewType == TYPE_HEADER) {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.header, parent, false)
            val vhHeader = ViewHolder(v, viewType)
            return vhHeader
        }
        return null
    }

    override fun onBindViewHolder(holder: NavAdapter.ViewHolder, position: Int) {
        if (holder.Holderid == 1) {
            if (position == selected_item) {
                holder.nav_holder?.setBackgroundResource(R.color.colorCardGray)
            }
            else {
                holder.nav_holder?.setBackgroundResource(R.color.colorCard)
            }
            holder.textView?.text = mNavTitles[position - 1]
            holder.imageView?.setImageResource(mIcons[position - 1])
        } else {
            holder.profile?.setImageResource(profile)
            holder.Name?.text = name
            holder.email?.text = email
        }
    }

    override fun getItemCount(): Int {
        return mNavTitles.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        if (isPositionHeader(position))
            return TYPE_HEADER
        return TYPE_ITEM
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    companion object {
        private val TYPE_HEADER = 0
        private val TYPE_ITEM = 1
    }

}
