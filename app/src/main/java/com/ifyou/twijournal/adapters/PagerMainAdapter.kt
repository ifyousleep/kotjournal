package com.ifyou.twijournal.adapters

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.View
import android.view.ViewGroup
import com.ifyou.twijournal.R
import com.ifyou.twijournal.fragments.MainFragment

class PagerMainAdapter(fm: FragmentManager, context: Context, fragments: Array<MainFragment>) : FragmentStatePagerAdapter(fm) {
    val pagers = arrayOf(context.getString(R.string.hours),
            context.getString(R.string.fresh),
            context.getString(R.string.week),
            context.getString(R.string.month))
    val fragment = fragments

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any?) {
        container.removeView(`object` as View?)
    }

    override fun getItem(position: Int): Fragment {
        return fragment[position]
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragmentIs = super.instantiateItem(container, position) as MainFragment
        fragment[position] = fragmentIs
        return fragmentIs
    }

    override fun getCount(): Int {
        return pagers.count()
    }

    override fun getPageTitle(position: Int): CharSequence {
        return pagers[position]
    }
}
