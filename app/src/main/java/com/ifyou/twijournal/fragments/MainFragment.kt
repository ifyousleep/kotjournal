package com.ifyou.twijournal.fragments

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.ifyou.twijournal.App
import com.ifyou.twijournal.R
import com.ifyou.twijournal.adapters.MainAdapter
import com.ifyou.twijournal.net.Post
import com.ifyou.twijournal.fragments.RxRetrofitFragment
import com.ifyou.twijournal.utils.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.fragment_main.view.*
import kotlinx.android.synthetic.main.retrofit_error.view.*
import org.jetbrains.anko.support.v4.withArguments
import rx.Observable

class MainFragment : RxRetrofitFragment() {

    private var adapter: MainAdapter? = null
    private var rv: LinearLayoutManager? = null
    private var offset = 0

    private var loading = true
    var pastVisiblesItems: Int = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0

    private var tweets: List<Any> = emptyList()

    companion object {
        fun newInstance(interval: String = "", listId: Int = 1): MainFragment {
            return MainFragment().withArguments("interval" to interval, "listId" to listId)
        }
    }

    fun scrollTop() {
        rv?.scrollToPosition(0)
    }

    override fun onGetObservable(): Observable<List<Post>> {
        return App.API().getFresh(interval = arguments.getString("interval"),
                listId = arguments.getInt("listId"),
                offset = offset)
    }

    override fun updateView(data: List<Any>) {
        adapter!!.updateTweetsList(data)
        loading = true
    }

    override fun onCreateErrorView(): View {
        val view = inflate(R.layout.retrofit_error)
        view.textError.text = getString(R.string.error)
        view.timeline_swipe_layout.setColorSchemeResources(R.color.colorPrimary)
        view.timeline_swipe_layout.setOnRefreshListener {
            offset = 0
            fetchData(offset)
        }
        return view
    }

    override fun onCreateSuccessView(data: List<Any>): View {
        tweets = data
        val view = inflate(R.layout.fragment_main)
        view.recyclerView.setHasFixedSize(true)
        rv = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        view.recyclerView.layoutManager = rv
        adapter = MainAdapter(activity, tweets)
        view.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    visibleItemCount = rv!!.childCount
                    totalItemCount = rv!!.itemCount
                    pastVisiblesItems = rv!!.findFirstVisibleItemPosition()
                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount - 4) {
                            loading = false
                            offset += 20
                            fetchData(offset)
                        }
                    }
                }
            }
        })
        view.recyclerView.adapter = adapter
        view.recyclerView.addItemDecoration(SimpleDividerItemDecoration(context))
        view.timeline_swipe.setColorSchemeResources(R.color.colorPrimary)
        view.timeline_swipe.setOnRefreshListener {
            offset = 0
            fetchData(offset)
        }
        return view
    }
}