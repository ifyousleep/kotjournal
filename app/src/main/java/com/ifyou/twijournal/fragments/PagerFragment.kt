package com.ifyou.twijournal.fragments

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ifyou.twijournal.R
import com.ifyou.twijournal.activities.MainActivity
import com.ifyou.twijournal.adapters.PagerClubAdapter
import com.ifyou.twijournal.adapters.PagerMainAdapter
import kotlinx.android.synthetic.main.pager_layout.*
import org.jetbrains.anko.support.v4.withArguments

class PagerFragment : Fragment() {

    private lateinit var listener: TabLayout.OnTabSelectedListener

    companion object {
        fun newInstance(current: Int, type: Int): PagerFragment {
            return PagerFragment().withArguments("current" to current, "type" to type)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.pager_layout, container, false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments.getInt("type", 0) == 0) {
            when (arguments.getInt("current")) {
                0 -> {
                    val array = arrayOf(MainFragment.newInstance("3hours"),
                            MainFragment.newInstance("fresh"),
                            MainFragment.newInstance("week"),
                            MainFragment.newInstance("month"))
                    viewPager.adapter = PagerMainAdapter(childFragmentManager, context, array)
                }
                1 -> {
                    val array = arrayOf(MainFragment.newInstance("3hours", 3),
                            MainFragment.newInstance("fresh", 3),
                            MainFragment.newInstance("week", 3),
                            MainFragment.newInstance("month", 3))
                    viewPager.adapter = PagerMainAdapter(childFragmentManager, context, array)
                }
                else -> {
                    val array = arrayOf(MainFragment.newInstance("3hours", 2),
                            MainFragment.newInstance("fresh", 2),
                            MainFragment.newInstance("week", 2),
                            MainFragment.newInstance("month", 2))
                    viewPager.adapter = PagerMainAdapter(childFragmentManager, context, array)
                }
            }
            viewPager.offscreenPageLimit = 4
            tabLayout.setupWithViewPager(viewPager)
            listener = object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    viewPager.currentItem = tab.position
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {

                }

                override fun onTabReselected(tab: TabLayout.Tab) {
                    val fragment = (viewPager.adapter as PagerMainAdapter).getItem(tab.position) as MainFragment
                    fragment.scrollTop()
                    if (activity is MainActivity) (activity as MainActivity).showAppBar()
                }
            }
            tabLayout.addOnTabSelectedListener(listener)
            viewPager.currentItem = 1
        } else {
            when (arguments.getInt("current")) {
                0 -> {
                    val array = arrayOf(ClubFragment.newInstance(0, "mainpage"),
                            ClubFragment.newInstance(0, "week"),
                            ClubFragment.newInstance(0, "editorial"),
                            ClubFragment.newInstance(0, "unadmitted"))
                    viewPager.adapter = PagerClubAdapter(childFragmentManager, context, array)
                }
            }
            viewPager.offscreenPageLimit = 4
            tabLayout.setupWithViewPager(viewPager)
            listener = object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    viewPager.currentItem = tab.position
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {

                }

                override fun onTabReselected(tab: TabLayout.Tab) {
                    val fragment = (viewPager.adapter as PagerClubAdapter).getItem(tab.position) as ClubFragment
                    fragment.scrollTop()
                    if (activity is MainActivity) (activity as MainActivity).showAppBar()
                }
            }
            tabLayout.addOnTabSelectedListener(listener)
            viewPager.currentItem = 0
        }
    }
}