package com.ifyou.twijournal.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.preference.ListPreference
import android.support.v7.preference.PreferenceFragmentCompat
import android.view.View
import com.bumptech.glide.Glide
import com.ifyou.twijournal.R
import com.ifyou.twijournal.activities.SettingsActivity
import com.ifyou.twijournal.utils.fullCacheDataSize
import com.ifyou.twijournal.utils.setNightMode
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class PreferenceFragment : PreferenceFragmentCompat() {

    private lateinit var theme: ListPreference
    private lateinit var font: ListPreference
    private lateinit var sharedPreferences: SharedPreferences

    companion object {
        val PREF_THEME = "pref_theme"
        val PREF_CLEAR_CACHE = "pref_clear_cache"
        val PREF_FONT = "pref_font"
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, p1: String?) {
        addPreferencesFromResource(R.xml.preferences)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPreferences = preferenceManager.sharedPreferences
        sharedPreferences.registerOnSharedPreferenceChangeListener(changeListener)
        theme = findPreference(PREF_THEME) as ListPreference
        theme.summary = theme.entry
        font = findPreference(PREF_FONT) as ListPreference
        font.summary = font.entry

        val clearCache = findPreference(PREF_CLEAR_CACHE)
        clearCache.summary = fullCacheDataSize(context)
        clearCache.setOnPreferenceClickListener {
            doAsync() {
                Glide.get(context).clearDiskCache()
                com.ifyou.twijournal.utils.clearCache(context)
                uiThread {
                    clearCache.summary = fullCacheDataSize(context)
                }
            }
            true
        }
    }

    private val changeListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
        when (key) {
            PREF_THEME -> {
                theme.summary = theme.entry
                setNightMode()
                if (activity is SettingsActivity) (activity as SettingsActivity).recreate()
            }
            PREF_FONT -> {
                font.summary = font.entry
                if (activity is SettingsActivity) (activity as SettingsActivity).recreate()
            }
        }
    }
}
