package com.ifyou.twijournal.net

import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface API {
    @GET("tweets")
    fun getFresh(@Query("listId") listId: Int = 1, @Query("interval") interval: String = "fresh"
                 , @Query("count") count: Int = 20, @Query("offset") offset: Int = 0): Observable<List<Post>>

    @GET("club")
    fun getClub(@Query("count") count: Int = 20, @Query("offset") offset: Int = 0
                , @Query("type") type: Int = 0, @Query("sortMode") sortMode: String = "mainpage"): Observable<List<Club>>

    /*@GET("club/item")
    fun getClubItem(@Query("entryId") entryId: Int = 1234) : Observable<ClubItemModel>*/
}