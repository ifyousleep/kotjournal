/*
package com.ifyou.twijournal.net

import java.util.*

data class ClubItemModel (
    var id: Int,
    var title: String,
    var url: String,
    var date: Int,
    var dateRFC: String,
    var author: AuthorItemModel,
    var publicAuthor: PublicAuthorItemModel,
    var type: Int,
    var intro: String,
    var cover: CoverItemModel,
    var externalLink: ExternalLinkItemModel,
    var inspiredByThis: Any,
    var entryJSON: String,
    var isReadMore: Boolean,
    var hits: Int,
    var likes: LikesItemModel,
    var commentsCount: Int,
    var isFavorited: Boolean,
    var userDevice: Int,
    var mobileAppUrl: String,
    var isDraft: Boolean,
    var isGold: Boolean,
    var isVotingActive: Boolean,
    var isWide: Boolean,
    var isAdvertising: Boolean,
    var isCommentsClosed: Boolean,
    var isStillUpdating: Boolean,
    var isComplexMarkup: Boolean,
    var isBigPicture: Boolean,
    var commentsPreview: List<Any> = ArrayList<Any>()
)

data class ExternalLinkItemModel (
    var data: Data,
    var domain: String,
    var url: String
)

data class LikesItemModel (
    var count: Int,
    var summ: Int,
    var isLiked: Int,
    var isHidden: Boolean,
    var hash: Any
)

data class PublicAuthorItemModel (
    var id: Int,
    var name: String,
    var profile_image_url: String,
    var profile_big_image_url: String,
    var url: String
)

data class SizeItemModel (
    var width: Int,
    var height: Int
)

data class AuthorItemModel (
    var id: Int,
    var name: String,
    var profile_image_url: String,
    var profile_big_image_url: String,
    var url: String
)

data class CoverItemModel (
    var type: Int,
    var additionalData: Any,
    var thumbnailUrl: String,
    var url: String,
    var size: SizeItemModel
)

data class Data (
    var url: String
)*/
