package com.ifyou.twijournal.net

data class Club(
        val id: Int,
        val title: String,
        val url: String,
        val date: Int,
        val dateRFC: String,
        val author: Author,
        val publicAuthor: PublicAuthor?,
        val type: Int,
        val intro: String,
        val cover: Cover?,
        val externalLink: Any,
        val inspiredByThis: Any,
        val isReadMore: Boolean,
        val hits: Int,
        val likes: Likes,
        val commentsCount: Int,
        val isFavorited: Boolean,
        val userDevice: Int,
        val mobileAppUrl: String,
        val isDraft: Boolean,
        val isGold: Boolean,
        val isVotingActive: Boolean,
        val isWide: Boolean,
        val isAdvertising: Boolean,
        val isCommentsClosed: Boolean,
        val isStillUpdating: Boolean,
        val isComplexMarkup: Boolean,
        val isBigPicture: Boolean
)

data class Likes(
        val count: Int,
        val summ: Int,
        val isLiked: Int,
        val isHidden: Boolean,
        val hash: Any
)

data class PublicAuthor(
        val id: Int,
        val name: String,
        val profile_image_url: String,
        val profileBigImageUrl: String,
        val url: String
)

data class Author(
        val id: Int,
        val name: String,
        val profile_image_url: String,
        val profileBigImageUrl: String,
        val url: String
)

data class Cover(
        val type: Int,
        val additionalData: Any,
        val thumbnailUrl: String,
        val url: String,
        val size: Size
)

data class Size(
        val width: Int,
        val height: Int
)