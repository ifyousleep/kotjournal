package com.ifyou.twijournal.net

import java.util.*

data class Post(
        val id: String,
        val text: String,
        val user: User,
        val retweet_count: Int,
        val favorite_count: Int,
        val has_media: Boolean,
        val media: List<Medium> = ArrayList(),
        val isFavorited: Boolean,
        val security_user_hash: String,
        val created_at: Long
)

data class User(
        val created_at: Int,
        val followers_count: Int,
        val friends_count: Int,
        val id: Long,
        val name: String,
        val profile_image_url: String,
        val profile_image_url_bigger: String,
        val screen_name: String,
        val statuses_count: Int
)

data class Medium(
        val type: Int,
        val thumbnail_url: String,
        val media_url: String,
        val thumbnail_width: Int,
        val thumbnail_height: Int,
        val ratio: Float
)