package com.ifyou.twijournal.utils

import android.content.Context
import android.content.ContextWrapper

class AudioServiceActivityLeak internal constructor(base: Context) : ContextWrapper(base) {
    override fun getSystemService(name: String): Any {
        if (Context.AUDIO_SERVICE == name) {
            return applicationContext.getSystemService(name)
        }
        return super.getSystemService(name)
    }

    companion object {
        fun preventLeakOf(base: Context): ContextWrapper {
            return AudioServiceActivityLeak(base)
        }
    }
}
