package com.ifyou.twijournal.utils

import java.text.SimpleDateFormat
import java.util.*

fun getDate(timeStamp: Long): String {
    try {
        val sdf = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault())
        val netDate = Date(timeStamp * 1000L)
        return sdf.format(netDate)
    } catch (ex: Exception) {
        return "xx"
    }
}