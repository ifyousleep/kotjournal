package com.ifyou.twijournal.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import java.io.File

fun ImageView.loadImage(context: Context, imagePath: String) {
    Glide.with(context)
            .load(imagePath)
            .into(this)
}

fun ImageView.loadImageTweet(context: Context, imagePath: String, res: Int) {
    Glide.with(context)
            .load(imagePath)
            .centerCrop()
            .placeholder(res)
            .into(this)
}

fun clearCache(context: Context) {
    try {
        val dir = context.cacheDir
        if (dir != null && dir.isDirectory) {
            deleteDir(dir)
        }
    } catch (e: Exception) {
        //Do nothing
    }

}

fun deleteDir(dir: File?): Boolean {
    if (dir != null && dir.isDirectory) {
        val children = dir.list()
        for (aChildren in children) {
            val success = deleteDir(File(dir, aChildren))
            if (!success) {
                return false
            }
        }
    }

    return dir != null && dir.delete()
}

private fun dirSize(dir: File): Long {
    if (dir.exists()) {
        var result: Long = 0
        val fileList = dir.listFiles()
        for (aFileList in fileList) {
            if (aFileList.isDirectory) {
                result += dirSize(aFileList)
            } else {
                result += aFileList.length()
            }
        }
        return result
    }
    return 0
}

fun fullCacheDataSize(context: Context): String {
    val finalSize: String
    var cache: Long = 0
    var extCache: Long = 0
    val finalResult: Double
    val mbFinalResult: Double

    val fileList = context.cacheDir.listFiles()
    for (aFileList in fileList) {
        if (aFileList.isDirectory) {
            cache += dirSize(aFileList)
        } else {
            cache += aFileList.length()
        }
    }
    try {
        var fileExtList = arrayOfNulls<File>(0)
        try {
            fileExtList = context.externalCacheDir.listFiles()
        } catch (e: NullPointerException) {
            //Do nothing
        }

        for (aFileExtList in fileExtList) {
            if (aFileExtList!!.isDirectory) {
                extCache += dirSize(aFileExtList)
            } else {
                extCache += aFileExtList.length()
            }
        }
    } catch (npe: NullPointerException) {

    }
    finalResult = ((cache + extCache) / 1000).toDouble()
    if (finalResult > 1001) {
        mbFinalResult = finalResult / 1000
        finalSize = String.format("%.2f", mbFinalResult) + " MB"
    } else {
        finalSize = String.format("%.2f", finalResult) + " KB"
    }
    return finalSize
}