package com.ifyou.twijournal.utils

import java.util.regex.Pattern

fun replaceWebLink(input: String): String {
    val regex = "\\[\\[(.+?)\\|\\|(http.+?)\\|\\|(.+?)\\]\\]"
    val p = Pattern.compile(regex)
    var id: String
    var name: String
    val m = p.matcher(input)
    val sb = StringBuffer()
    while (m.find()) {
        val mr = m.toMatchResult()
        id = mr.group(2)
        name = mr.group(3)
        m.appendReplacement(sb, "<a href='$id'>$name</a>")
    }
    m.appendTail(sb)
    return replaceNameLink(sb.toString())
}

fun replaceNameLink(input: String): String {
    val regex = "\\[\\[(.+?)\\|\\|(.+?)\\|\\|(@.+?)\\]\\]"
    val p = Pattern.compile(regex)
    var id: String
    var name: String
    val m = p.matcher(input)
    val sb = StringBuffer()
    while (m.find()) {
        val mr = m.toMatchResult()
        id = mr.group(1)
        name = mr.group(3)
        m.appendReplacement(sb, "<a href='$id'>$name</a>")
    }
    m.appendTail(sb)
    return sb.toString()
}