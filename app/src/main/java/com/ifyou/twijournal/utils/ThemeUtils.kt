package com.ifyou.twijournal.utils

import android.content.SharedPreferences
import android.os.Build
import android.support.annotation.AnimRes
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatDelegate
import android.support.v7.preference.PreferenceManager
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.ifyou.twijournal.appContext

fun setNightMode() {
    val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext)
    val theme = sharedPreferences.getString("pref_theme", "0")
    when (theme.toInt()) {
        0 -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        1 -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        2 -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_AUTO)
        3 -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
    }
}


fun View.fadeIn(animate: Boolean = true) {
    visibility = View.VISIBLE
    if (animate) {
        fade(1F, android.R.anim.fade_in)
    } else {
        ViewCompat.setAlpha(this, 1F)
    }
}

fun View.fadeOut(animate: Boolean = true) {
    if (animate) {
        fade(0F, android.R.anim.fade_out)
    } else {
        visibility = View.GONE
        ViewCompat.setAlpha(this, 0F)
    }
}

private fun View.fade(alpha: Float, @AnimRes animRes: Int) {
    if (Build.VERSION.SDK_INT >= 14) {
        val mediumAnimTime =
                resources.getInteger(android.R.integer.config_mediumAnimTime).toLong()
        ViewCompat.animate(this)
                .alpha(alpha)
                .setDuration(mediumAnimTime)
                .start()
    } else {
        val animation = AnimationUtils.loadAnimation(context, animRes)
        animation.setAnimationListener(FadeAnimationListener(this, alpha > 0F))
        startAnimation(animation)
    }
}

private class FadeAnimationListener(val view: View, val visible: Boolean) :
        AnimationListenerAdapter {

    override fun onAnimationEnd(animation: Animation) {
        view.visibility = if (visible) View.VISIBLE else View.GONE
    }
}

interface AnimationListenerAdapter : Animation.AnimationListener {

    override fun onAnimationRepeat(animation: Animation) {
    }

    override fun onAnimationEnd(animation: Animation) {
    }

    override fun onAnimationStart(animation: Animation) {
    }
}
